<?php
    namespace App\Enumeration;

    class Role {
        public static $ADMIN = 1;
        public static $EMPLOYEE = 2;
        public static $BUYER = 3;
    }