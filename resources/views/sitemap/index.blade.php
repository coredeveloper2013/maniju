<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{{ route('sitemap_static') }}</loc>
    </sitemap>

    <sitemap>
        <loc>{{ route('sitemap_vendors') }}</loc>
        @if ($vendorsLastModify != null)
            <lastmod>{{ $vendorsLastModify->updated_at->tz('UTC')->toAtomString() }}</lastmod>
        @endif
    </sitemap>

    <sitemap>
        <loc>{{ route('sitemap_categories') }}</loc>
        @if ($categoriesModify != null)
            <lastmod>{{ $categoriesModify->updated_at->tz('UTC')->toAtomString() }}</lastmod>
        @endif
    </sitemap>
</sitemapindex>