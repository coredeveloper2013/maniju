<?php
    use App\Enumeration\Role;
    use App\Enumeration\Availability;
?>

@extends('layouts.home_layout')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fotorama.css') }}">
    <style>
    .sub_category_menu {
        margin-left: 0px;
        padding-left: 5px;
        border-left: 1px solid #eee;
    }
    .product_filter_right ul li a span.right_errow {
        display: inline-block;
        width: 0;
        height: 0;
        border-top: 4px solid transparent;
        border-bottom: 4px solid transparent;
        border-right: 4px solid #666;
        margin: 5px 0;
        border-left: 0;
    }
    </style>
@stop

@section('content')
    <div class="container category-page">
         <!--START BANNER SECTION-->
        <section class="banner_area common_banner clearfix">
            <div class="row">
                <div class="col-md-12 custom_padding_9">
                    @if(count($top_notification_banner_module) != 0)
                    @php
                        $imageUrl = asset($top_notification_banner_module[0]->image_path);
                    @endphp
                    <div class="banner_top" style="background-image: url({{$imageUrl}});height: 15vh;background-size: 100% 100%;"></div>
                    @endif
                </div>
            </div>
        </section>
        <!-- END BANNER SECTION-->

        <!--START BREDCRUMS SECTION -->
        <section class="breadcrumbs_area">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">@php echo $category->name; @endphp</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </section>
        <!--END BREDCRUMS SECTION -->

        <!--START CATEGORY SECTION-->
        <section class="appoinment_area common_content_area" id='main_app'>
            <div class="row">
                <div class="col-md-2 custom_padding_9 for_desktop d-none d-lg-block">
                    <div class="common_left_menu">
                        <ul>
                            <li><a href="{{ route('new_arrival_page') }}">New Arrival</a></li>
                            <li><a href="{{ route('best_selling_page') }}">Best Selling</a></li>
                            <ul>
                                @foreach($default_categories as $cat)
                                <li><a href="{{ route('category_page', ['category' => changeSpecialChar($cat['name'])]) }}">{{ $cat['name'] }}</a></li>
                                @if(count($cat['subCategories'])>0)
                                    @foreach($cat['subCategories'] as $d_sub)
                                        <li class="sub_category_menu"><a href="{{ route('second_category', ['category' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($cat['name'])]) }}">- {{ $d_sub['name'] }}</a></li>
                                    @endforeach
                                @endif
                                @endforeach
                            </ul>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-10 col-md-12">
                    <div class="row">
                        @if(Request::get('sort_by')!='')
                            @php ($url = $url.'?sort_by='.Request::get('sort_by').'&')
                        @else
                            @php ($url = $url.'?')
                        @endif

                        @if(!empty($items) && count($items)>0)
                        <div class="col-md-12 category-custom_padding">
                            <div class="product_filter clearfix">
                                <div class="product_filter_left">
                                    <form method="get">
                                        <select class="form-control sort_by" name="sort_by" onchange="this.form.submit()">
                                            <option value="">Sort By</option>
                                            <option value="low_to_high" {{ (Request::get('sort_by')=='low_to_high')?'selected':''}}>Price: Low - High</option>
                                            <option value="high_to_low" {{ (Request::get('sort_by')=='high_to_low')?'selected':''}}>Price: High - Low</option>
                                        </select>
                                    </form>
                                </div>
                                <div class="product_filter_right">
                                    <ul>
                                        <li><a href="{{ ($items->currentPage()!=1)?url($url.'page='.($items->currentPage()-1)):'#' }}"><span class="right_errow"></span></a></li>
                                        <li>{{ $items->currentPage() }}  of {{ $items->lastPage() }}</li>
                                        <li><a href="{{ ($items->lastPage()!=$items->currentPage())?url($url.'page='.($items->currentPage()+1)):'#' }}"><span></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                @foreach($items as $record)
                                <div class="col-6 col-md-4 col-lg-3 category-custom_padding">
                                    <div class="product_inner text-center">
                                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                            <a href="{{ route('product_single_page', $record->slug) }}">
                                                @if(count($record->images)==0)
                                                    <img  src="{{asset('images/no-image.png')}}" alt="" class="img-fluid">
                                                @else
                                                <div class="owl-carousel owl-theme owl_product" id="{{ $record->id }}">
                                                    @foreach($record->images as $img)
                                                    <div class="item"><img src="{{ URL::to('/').'/'.$img['image_path'] }}" alt="" class="img-fluid">
                                                        @if($record->default_parent_category==5)
                                                        <div class="owl-overlay-text">Preorder</div>
                                                        @endif
                                                    </div>
                                                    @endforeach
                                                </div>
                                                @endif
                                            </a>

                                            <h2><a href="{{ route('product_single_page', $record->slug) }}">{{$record->name}}</a></h2>
                                            <p>${{$record->price}}</p>

                                            @if(count($record->images)>0)
                                            <div class="owl-dots" id="dotCustom{{ $record->id }}">
                                                @foreach($record->images as $img)
                                                    @if($img->color!=null)
                                                    <span onclick="clickDot({{ $record->id }})"><img src="{{ URL::to('/').'/'.$img->color->image_path }}" alt="{{$img->color->name}}" class="dot-img"></span>
                                                    @endif
                                                @endforeach
                                            </div>
                                            @endif
                                        @else
                                            <a href="'{{ route('product_single_page', $record->slug) }}">
                                                <img src="{{$defaultItemImage_path}}" alt="" class="img-fluid">
                                            </a>
                                            <h2><a href="'{{ route('product_single_page', $record->slug) }}">{{$record->name}}</a></h2>
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="col-md-12 category-custom_padding">
                            <div class="product_filter clearfix">
                                <div class="product_filter_left">
                                    <form method="get">
                                        <select class="form-control sort_by" name="sort_by" onchange="this.form.submit()">
                                            <option value="">Sort By</option>
                                            <option value="low_to_high" {{ (Request::get('sort_by')=='low_to_high')?'selected':''}}>Price: Low - High</option>
                                            <option value="high_to_low" {{ (Request::get('sort_by')=='high_to_low')?'selected':''}}>Price: High - Low</option>
                                        </select>
                                    </form>
                                </div>
                                <div class="product_filter_right">
                                    <ul>
                                        <li><a href="{{ ($items->currentPage()!=1)?url($url.'page='.($items->currentPage()-1)):'#' }}"><span class="right_errow"></span></a></li>
                                        <li>{{ $items->currentPage() }}  of {{ $items->lastPage() }}</li>
                                        <li><a href="{{ ($items->lastPage()!=$items->currentPage())?url($url.'page='.($items->currentPage()+1)):'#' }}"><span></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="col-md-12 text-center">
                            <h3>Item not found in this category!</h3>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </section>
        <!-- END CATEGORY SECTION-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/fotorama.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jqueryCookie/jquery.cookie-1.4.1.min.js') }}"></script>
    <script>
        $(function () {
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $( document ).ready(function() {
                $(".owl_product").owlCarousel({
                    loop:true,
                    nav:false,
                    margin:10,
                    //dotsContainer: '.owl-dots',
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1000:{
                            items:1
                        }
                    }
                });
            });
        });

        function clickDot(id) {
            $('#dotCustom'+id).on('click', 'span', function(e) {
                $('#'+id).trigger('to.owl.carousel', [$(this).index(), 300]);
            });
        }
    </script>
@stop
