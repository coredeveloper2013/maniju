@extends('layouts.home_layout')

@section('additionalCSS')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')

    <section class="my_account_area common_top_margin common_content_area">
        <div class="container custom_container">
            <div class="row">
                <div class="col-md-2 d-none d-lg-block">
                    <div class="my_accout_menu common_left_menu">
                     @include('buyer.profile.menu')
                    </div>
                </div>
                <div class="col-lg-10 col-md-12">
                    <div class="my_account_content my_wishlist_area">
                        <div class="clearfix">&nbsp;</div>
                        <div class="myaccount_title">
                            <h2>My Wishlist</h2>
                        </div>
                        <div class="my_info_area my_wishlist_area">
                            {{-- <button class="btn_common">add all to bag</button>
                            <button class="btn_common">share wishlist</button>
                            <p>Confetti Ready has been added to your wishlist. Click <a href="{{ route('product_page') }}">here</a> to continue shopping.</p> --}}

                            <div class="row">
                                @if ( ! $items->isEmpty() )
                                    @foreach ( $items as $product )
                                    <div class="col-lg-4" id="{{ $product->id }}">
                                        <div class="product_wishlist text-center">
                                            {{-- <button onclick="myFunction();"><img src="{{ asset('images/cross_ic.png') }}" alt="No image"></button> --}}
                                            @if ( isset($product->images) && count($product->images) > 0 )
                                                <a href="{{ route('product_single_page', $product->slug) }}"><img src="{{ asset('/' . $product->images[0]['image_path']) }}" alt="" class="img-fluid"></a>
                                            @else
                                                <a href="{{ route('product_single_page', $product->slug) }}"><img src="{{ asset('/images/no-image.png') }}" alt="" class="img-fluid"></a>
                                            @endif
                                            <div>
                                                <h2>{{ $product->name }}</h2>
                                                <p class="price">
                                                    {{--  @if($product->orig_price != null && $product->orig_price != '')
                                                    <span>${{$product->orig_price}}</span>
                                                    @endif  --}}
                                                    ${{ $product->price }}
                                                </p>
                                                <button class="add_cart_btn add_to_cart" id="id_{{$product->id}}">add to bag</button>
                                                <span class="add_cart_btn remove_wishlist" id="remove_{{$product->id}}" title="Remove item" data-id="{{ $product->id }}">X</span>
                                            </div>
                                            @if ( $product->rating['count'] > 0 )
                                                <div class="product_star_rating">
                                                    <span class="customer_rateYo" data-type="{{ $product->rating['rate'] }}"></span>
                                                    <span class="average_rating">({{ $product->rating['count'] }})</span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                    <p class="no_wishlist col-sm-12 text-center">You have no items in your wishlist.</p>
                                @endif
                            </div>
                        </div>



                        <div class="modal fade" id="color-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
                        aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="modalLabelLarge">Add Item</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>

                                    <div class="modal-body">
                                        <form id="form_new_item">

                                        </form>
                                    </div>

                                    <div class="modal-footer">
                                        <button class="add_cart_btn" id="btnItemAdd">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <template id="template-table">
                            <div class="modal-item">
                                <h5 class="template-item-name"></h5>
                                <table class="table table-bordered item_colors_table">

                                </table>
                            </div>
                        </template>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
@section('additionalJS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<script>
    //Make sure that the dom is ready
    $(function () {
        $(".customer_rateYo").each(function(){
            var rating = $(this).attr('data-type');
            $(this).rateYo({
                starWidth: "17px",
                rating: rating,
                readOnly: true,
                normalFill: "#cfcfcf",
                ratedFill: "#000"
            });
        });
    });


    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': '{!! csrf_token() !!}'
            }
        });

        var message = '{{ session('message') }}';

        if (message != '')
            toastr.success(message);

        $('.add_to_cart').click(function (e) {
            e.preventDefault();

            var target_id = $(this).attr('id').split('_')[1];

            var ids = [];

            // $('.checkbox_item').each(function () {
            //     if ($(this).is(':checked')) {
            //         ids.push($(this).data('id'));
            //     }
            // });

            ids.push(target_id);

            if (ids.length > 0) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('wishlist_item_details') }}",
                    data: {id: ids}
                }).done(function (products) {
                    $('#form_new_item').html('');

                    //console.log(products);

                    $.each(products, function (i, product) {
                        var html = $('#template-table').html();
                        var item = $(html);

                        if(product.min_qty)
                        item.find('.template-item-name').html(product.style_no + ' - Min Qty: ' + product.min_qty);
                        else
                            item.find('.template-item-name').html(product.style_no);

                        $.each(product.colors, function (ci, color) {
                            if (color.image == ''){
                                var pack_item = '', pack = 0;
                                for(pack=1; pack<=10 ; pack++){
                                    if(product.pack.hasOwnProperty('pack' +pack) && product.pack['pack' +pack] !== null ){
                                        pack_item += product.pack['pack' +pack] + ' '
                                    }
                                }
                                item.find('.item_colors_table').append('<tr><td>'+ pack_item +'</td><th>' + color.name + '</th><td><input name="colors[]" class="input_color" type="text"><input type="hidden" name="ids[]" value="' + product.id + '"><input type="hidden" name="colorIds[]" value="' + color.id + '"></td></tr>');
                            }
                            else
                                item.find('.item_colors_table').append('<tr><td><img src="' + color.image + '" width="30px"></td><th>' + color.name + '</th><td><input name="colors[]" class="input_color" type="text"><input type="hidden" name="ids[]" value="' + product.id + '"><input type="hidden" name="colorIds[]" value="' + color.id + '"></td></tr>');

                        });


                        $('#form_new_item').append(item);
                    });

                    $('#color-modal').modal('show');

                    /*$('#item_colors_table').html('');

                     $.each(product.colors, function (i, color) {
                     $('#item_colors_table').append('<tr><th>' + color.name + '</th><td><input name="colors[' + color.id + ']" class="input_color" type="text"></td></tr>');
                     });*/
                });
            }
        });

        $('#btnItemAdd').click(function () {
            var error = false;

            $('.input_color').each(function () {
                if (error)
                    return;

                var count = $(this).val();

                if (count != '' && !isInt(count)) {
                    error = true;
                    return alert('Invalid input.');
                }
            });
            console.log($('#form_new_item').serialize());


            if (!error) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('wishlist_add_to_cart') }}",
                    data: $('#form_new_item').serialize()
                }).done(function (data) {
                    if (data.success)
                        location.reload();
                    else{
                        //console.log(data)
                        alert(data.message);
                    }
                        //alert(data.message);
                });
            }
        });

        function isInt(value) {
            return !isNaN(value) && (function (x) {
                return (x | 0) === x;
            })(parseFloat(value))
        }

        $('.remove_wishlist').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('id').split('_')[1];

            $.ajax({
                method: "POST",
                url: "{{ route('remove_from_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Removed from Wishlist.');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            });
        });
    });
</script>
@stop
