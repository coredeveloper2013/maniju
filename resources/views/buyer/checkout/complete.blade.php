@extends('layouts.home_layout')

@section('content')
<div class="container">
    <section class="shipping_cart_area text-center">
        {{--<div class="row">
            <div class="col-md-12">
                <div class="checkout-steps">
                    <a class="active">5. Complete</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>4. Review</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>3. Payment</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>2. Shipping Method</a>
                    <a class="completed"><span class="step-indicator icon-circle-check"></span><span class="angle"></span>1. Address</a>
                </div>
            </div>
        </div>--}}

        <h3>Thank you for your order</h3>
        <p>
            <b>Your Order Number is: </b>

            <a class="theme-anchor-color" href="{{ route('show_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a>&nbsp;
        </p>

        <p>We will proceed your order soon.</p>
    </section>
</div>
@stop
