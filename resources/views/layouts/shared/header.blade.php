<?php use App\Enumeration\Role; ?>
<!-- =========================
    START HEADER SECTION
============================== -->
<style>
    .header_top_bg_dynamic{
        background-color: #{{ isset($header_bg_color) ? $header_bg_color : '' }}
    }
    .header_top_bg_dynamic .header_search input, .header_search input:focus{
        color: #{{ isset($header_font_color) ? $header_font_color : '' }}
    }
    .header_top_bg_dynamic .header_right ul li a{
        color: #{{ isset($header_font_color) ? $header_font_color : '' }}
    }
</style>
<header class="header_area">
    <div class="header_desktop">
        <div class="header_top {{ isset($header_bg_color) ? 'header_top_bg_dynamic' : '' }}">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="header_search">
                            <form method="get" action="{{ route('search') }}">
                            <input type="text" name="s" placeholder="">
                            <label>Search</label>
                            <button><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                        <div class="header_right">
                            <ul>
                                {{-- <li><a href="{{ route('show_cart') }}">CART</a></li> --}}
                                <li><a href="{{ route('buyer_show_overview') }}">MY ACCOUNT</a></li>
                                <li><a href="{{ route('logout_buyer_get') }}">LOGOUT</a></li>
                                <li><a href="{{ route('show_cart') }}"><img src="{{ asset('themes/front/images/cart.png') }}" alt=""> <span>{{ count($cart_items)-1 }}</span></a></li>
                            </ul>
                        </div>
                        @else
                        <div class="header_right">
                            <ul>
                                {{-- <li><a data-toggle="modal" data-target="#headerModal">CART</a></li> --}}
                                <li><a href="{{ route('buyer_show_overview') }}">MY ACCOUNT</a></li>
                                <li><a data-toggle="modal" data-target="#headerModal"><img src="{{ asset('themes/front/images/cart.png') }}" alt=""> <span>0</span></a></li>
                            </ul>
                        </div>

                        @section('headerModal')
                        <div class="modal fade" id="headerModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">System Alert</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">Please login first.</div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @stop
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main_logo">
                        <a href="{{ route('home') }}" class="navbar-brand">
                            <img src="{{ $white_logo_path }}" alt="" class="img-responsive white_logo">
                        </a>
                    </div>
                    <nav class="main_menu">
                        <ul>
                        <li><a href="{{ route('new_arrival_page') }}">New Arrival</a></li>
                            @foreach($default_categories as $cat)
                                <li class="main-menu-li">
                                    <a href="{{ route('category_page', ['category' => changeSpecialChar($cat['name'])]) }}">{{ $cat['name'] }}</a>
                                    <?php
                                    $subIds = [];

                                    foreach ($cat['subCategories'] as $d_sub)
                                        $subIds[] = $d_sub['id'];
                                    ?>
                                    @if(count($cat['subCategories'])>0)
                                    <ul class="dropdown-menu __custom_drop_main">
                                        @foreach($cat['subCategories'] as $d_sub)
                                            <li><a href="{{ route('second_category', ['category' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($cat['name'])]) }}">{{ $d_sub['name'] }}</a></li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="header_mobile clearfix">
        <div class="header_left_mobile">
            <ul>
                <li>
                    <div class="showhide">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </li>
                {{-- <li>
                    <div class="mobile_location">
                        <a href="#"><i class="fas fa-map-marker-alt"></i></a>
                    </div>
                </li> --}}
            </ul>
        </div>
        <div class="mobile_logo">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ $white_logo_path }}" alt="" class="img-responsive white_logo_mobile">
            </a>
        </div>
        <div class="header_right_mobile">
            <ul>
                <li><i class="fas fa-search"></i></li>
                
                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                <li><a href="{{ route('show_cart') }}"><img src="{{ asset('themes/front/images/cart.png') }}" alt=""> <span>{{ count($cart_items)-1 }}</span></a></li>
                @else
                <li><a data-toggle="modal" data-target="#headerModal" href="#"><img src="{{ asset('themes/front/images/cart.png') }}" alt=""> <span>0</span></a></li>
                @endif
            </ul>
        </div>
        <div class="header_search_mobile clearfix">
            <form method="get" action="{{ route('search') }}">
                <input type="text" name="s" class="form-control" placeholder="Search">
            </form>
        </div>
    </div>

    <div class="mobile_overlay"></div>
    <div class="mobile_menu">
        <div class="top_menu_list clearfix">
            <h2>
                <a href="{{ route('buyer_show_overview') }}">Account</a> <span class="ic_c_mb"><img src="{{ asset('themes/front/images/cross_ic.png') }}" alt=""></span>
            </h2>
        </div>
        <div class="menu-list clearfix">
            <ul id="menu-content" class="menu-content">
                @if( isset(auth()->user()->active) && auth()->user()->active == 1 )
                <li data-toggle="collapse" data-target="#myaccount" class="with_collapse collapsed"><a href="#">Overview</a></li>
                <ul id="myaccount" class="collapse clearfix">
                    <li><a href="{{ route('buyer_show_overview') }}">My Profile</a></li>
                    <li><a href="{{ route('buyer_my_information') }}">My Information</a></li>
                    <li><a href="{{ route('buyer_billing') }}">Credit cards and billing</a></li>
                    <li><a href="{{ route('buyer_show_orders') }}">Order History</a></li>
                    <li><a href="{{ route('view_wishlist') }}">My Wishlist</a></li>
                    <li><a href="{{ route('logout_buyer_get') }}">Logout</a></li>
                </ul>
                @endif
                <li><a href="{{ route('new_arrival_page') }}">New Arrival</a></li>
                <li><a href="{{ route('best_selling_page') }}">Best Selling</a></li>
                @foreach($default_categories as $cat)
                    @if(count($cat['subCategories']) > 0)
                        <li data-toggle="collapse" data-target="#cat{{$cat['id']}}" class="with_collapse collapsed"><a href="#">{{ $cat['name'] }}</a></li>
                        <ul id="cat{{$cat['id']}}" class="collapse clearfix">
                        @foreach($cat['subCategories'] as $subCat)
                            <li class="mobile-list-item">
                                <a href="{{ route('second_category', ['parent'=>$cat['slug'], 'category'=>$subCat['slug']]) }}"
                                    class="mobile-list-item-link"> {{$subCat['name']}} </a>
                            </li>
                        @endforeach
                        </ul>
                    @else
                        <li class="no-before"><a href="{{ route('category_page', $cat['slug']) }}">{{ $cat['name'] }}</a></li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>

</header>

<!-- =========================
        END HEADER SECTION
    ============================== -->
