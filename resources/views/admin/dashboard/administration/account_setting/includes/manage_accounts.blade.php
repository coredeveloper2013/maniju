<?php use App\Enumeration\Permissions; ?>

<div class="row" id="addBtnRow">
    <div class="col-md-12">
        <button class="btn btn-primary" id="btnAddNewAccount">Add New Account</button>
    </div>
</div>

<div class="row d-none" id="addEditAccountRow">
    <div class="col-md-12" style="border: 1px solid black">
        <h3><span id="addEditTitle"></span></h3>

        <div class="form-group row">
            <div class="col-lg-1">
                <label for="status" class="col-form-label">Status</label>
            </div>

            <div class="col-lg-5">
                <label for="statusActive" class="custom-control custom-radio">
                    <input id="statusActive" name="status" type="radio" class="custom-control-input"
                           value="1" checked>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Active</span>
                </label>
                <label for="statusInactive" class="custom-control custom-radio signin_radio4">
                    <input id="statusInactive" name="status" type="radio" class="custom-control-input" value="0">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Inactive</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-1">
                <label class="col-form-label">First & Last Name</label>
            </div>

            <div class="col-lg-2">
                <input type="text" id="firstName" class="form-control" name="firstName">
            </div>

            <div class="col-lg-2">
                <input type="text" id="lastName" class="form-control" name="lastName">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-1">
                <label class="col-form-label">User ID</label>
            </div>

            <div class="col-lg-4">
                <input type="text" id="userId" class="form-control" name="userId">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-1">
                <label class="col-form-label">Password</label>
            </div>

            <div class="col-lg-4">
                <input type="password" id="password" class="form-control" name="password">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-1">
                <label for="default" class="col-form-label">Permissions</label>
            </div>

            <div class="col-lg-10">
                <div class="checkbox-inline">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$ITEMS }}">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Items</span>
                    </label>

                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$ORDERS }}">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Orders</span>
                    </label>

                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$CUSTOMERS }}">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Customers</span>
                    </label>

                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$MARKETING_TOOLS }}">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Marketing Tools</span>
                    </label>

                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$FEEDBACK }}">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Feedback</span>
                    </label>

                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$STATISTICS }}">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Statistics</span>
                    </label>

                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$MESSAGES }}">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Messages</span>
                    </label>

                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$ADMIN }}">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Admin</span>
                    </label>

                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$PAYMENT }}">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Payment</span>
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-1">
                <label for="default" class="col-form-label">Refund Access</label>
            </div>

            <div class="col-lg-5">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input permission" value="{{ Permissions::$REFUND }}">
                    <span class="custom-control-indicator"></span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-12 text-right">
                <button class="btn btn-default" id="btnCancelAccount">Cancel</button>
                <button id="btnAddAccount" class="btn btn-primary">Add</button>
                <button id="btnUpdateAccount" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Date Created</th>
                    <th>Name</th>
                    <th>Level</th>
                    <th>User ID</th>
                    <th>Last Login Date</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody id="accountsBody">
                <tr>
                    <td>{{ date('m/d/Y', strtotime($user->created_at)) }}</td>
                    <td>{{ $user->first_name.' '.$user->last_name }}</td>
                    <td>Master</td>
                    <td>{{ $user->user_id }}</td>
                    <td>{{ $user->last_login != null ? date('m/d/Y', strtotime($user->last_login)) : '' }}</td>
                    <td>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" value="1" checked disabled>
                            <span class="custom-control-indicator"></span>
                        </label>
                    </td>
                    <td></td>
                </tr>

                @foreach($users as $item)
                    <tr>
                        <td><span class="date_created">{{ date('m/d/Y', strtotime($item->created_at)) }}</span></td>
                        <td><span class="name">{{ $item->first_name.' '.$item->last_name }}</span></td>
                        <td><span class="level">Lower Level</span></td>
                        <td><span class="userId">{{ $item->user_id }}</span></td>
                        <td>{{ $item->last_login != null ? date('m/d/Y', strtotime($item->last_login)) : '' }}</td>
                        <td>
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input status" data-id="{{ $item->id }}" value="1" {{ $item->active == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>
                        </td>
                        <td>
                            <a class="btnEdit" role="button" data-id="{{ $item->id }}" data-index="{{ $loop->index }}" style="color: blue">Edit</a> |
                            <a class="btnDelete" role="button" data-id="{{ $item->id }}" data-index="{{ $loop->index }}" style="color: red">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<template id="accountItem">
    <tr>
        <td><span class="date_created"></span></td>
        <td><span class="name"></span></td>
        <td><span class="level">Lower Level</span></td>
        <td><span class="userId"></span></td>
        <td></td>
        <td>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input status" value="1">
                <span class="custom-control-indicator"></span>
            </label>
        </td>
        <td>
            <a class="btnEdit" role="button" style="color: blue">Edit</a> |
            <a class="btnDelete" role="button" style="color: red">Delete</a>
        </td>
    </tr>
</template>

<div class="modal fade" id="accountDeleteModal" role="dialog" aria-labelledby="accountDeleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title text-white" id="accountDeleteModal">Delete</h4>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure want to delete?
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn  btn-default" data-dismiss="modal">Close</button>
                <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
            </div>
        </div>
    </div>
    <!--- end modals-->
</div>
